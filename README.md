
# Animal Flocking Unity Scripts

This is the code used for the Animal Flocking System showcased on my portfolio site. I created the system as part of a final-year uni project.

Entities in a flock evaluate their environment and apply a set of behaviours to determine where they should move. These behaviours are individually very simplistic, but can be combined to create complex emergent patterns.

This project is based on Craig Reynold's [Boids](https://www.red3d.com/cwr/boids/) concept from 1986.

---

## How it works

1. The `FlockManager` script is placed on an empty GameObject within a Unity scene.
	* This allows flock parameters (behaviour weighting, speed etc) to be set within the Unity scene before runtime.
2. Any child GameObjects with a rigidbody and the `FlockAgent` script are treated as part of the flock.
	* The `FlockAgent` script sets up behaviours for the agent when the scene is loaded.
	* During runtime, each agent runs through its respective behaviours at a regular interval
3. Depending on the behaviours that are set and their weightings, the flock will react to stimuli and prioritise goals in different manners.

For the purposes of this project, the flock only evaluates objects on a 2D plane, however the code does support evaulation in three dimensions.

---

## Behaviours

There are three core behaviours that allow the flock to move as a group. By combining the results of these evaulations, a pattern emerges in which a group of agents will move together while trying to maintain their distance between each other.

1. Alignment: Move in the direction of the average rotation of neighbours.
2. Separation: Move away from neighbours that are deemed too close.
3. Cohesion: Move towards the average position of the group.


There are also additional behaviours that were written for the project:

* Collision Avoidance: Uses the separation behaviour, but instead searches for walls.
* Seek: Uses the cohesion behaviour, but instead travels towards for a specified target.
* Arrival: The agent comes to a stop when it has arrived at a Seek target.
* Flee: Uses the separation behaviour, but instead searches for specified objects.
* Evade: The agent predicts where a specified object is moving to based on its velocity and flees in the opposite direction
* Pursue: As Evade, but seeks where the target will be instead.
* Noise: Adds a slight jitter to the agent movement to stop the flock movement from looking robotic.



