/// <summary>
/// Public repository of tags that the flocking mechanisms require
/// </summary>
public static class Tags
{
    public const string ENTITYTAG = "Entity";

    public const string WALLTAG = "Wall";

    public const string SEEKTARGETTAG = "Seek Target";

    public const string FLEETARGETTAG = "Flee Target";

    public const string EVADETARGETTAG = "Evade Target";

    public const string PURSUETARGETTAG = "Pursue Target";

    public const string ARRIVALTARGETTAG = "Arrival Target";
}
