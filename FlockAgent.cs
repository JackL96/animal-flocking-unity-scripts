using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Allows a GameObject to act as a flock agent, controlled by the parent manager
/// </summary>
public class FlockAgent : MonoBehaviour
{
    /// <summary>
    /// Reference to the parent FlockManager
    /// </summary>
    private FlockManager _parent;

    /// <summary>
    /// Reference to the Agent's own rigidbody
    /// </summary>
    private Rigidbody _rigidbody;

    /// <summary>
    /// Storage for all the flocking behaviours that will be applied to the rigidbody velocity
    /// </summary>
    private readonly List<IBehaviour> _behaviours = new List<IBehaviour>();

    /// <summary>
    /// Initialisation for the Agent within the Unity scene
    /// </summary>
    private void Start()
    {
        _parent = GetComponentInParent<FlockManager>();

        _rigidbody = GetComponent<Rigidbody>();

        // Alignment
        _behaviours.Add(new AlignmentBehaviour(() => _parent.AlignmentRange, () => _parent.AlignmentWeight));
        // Separation
        _behaviours.Add(new SeparationBehaviour(() => _parent.SeparationRange, () => _parent.SeparationWeight, Tags.ENTITYTAG));
        // Cohesion
        _behaviours.Add(new CohesionBehaviour(() => _parent.AlignmentRange, () => _parent.CohesionWeight, Tags.ENTITYTAG));
        // Noise
        _behaviours.Add(new NoiseBehaviour(() => _parent.NoiseWeight));
        // Collision Avoidance
        _behaviours.Add(new SeparationBehaviour(() => _parent.CollisionAvoidanceRange, () => _parent.CollisionAvoidanceWeight, Tags.WALLTAG));

        // Seek
        _behaviours.Add(new CohesionBehaviour(() => _parent.SeekRange, () => _parent.SeekWeight, Tags.SEEKTARGETTAG));
        // Flee
        _behaviours.Add(new SeparationBehaviour(() => _parent.FleeRange, () => _parent.FleeWeight, Tags.FLEETARGETTAG));
        // Evade
        _behaviours.Add(new EvadeBehaviour(() => _parent.EvadeRange, () => _parent.EvadeWeight, () => _parent.FramePredictionCount));
        // Pursue
        _behaviours.Add(new PursuitBehaviour(() => _parent.PursueRange, () => _parent.PursueWeight, () => _parent.FramePredictionCount));
        // Arrival
        _behaviours.Add(new ArrivalBehaviour(() => _parent.ArrivalStopRange, () => _parent.ArrivalStopWeight, Tags.ARRIVALTARGETTAG));
        _behaviours.Add(new CohesionBehaviour(() => _parent.ArrivalRange, () => _parent.ArrivalWeight, Tags.ARRIVALTARGETTAG));
    }

    /// <summary>
    /// Updates velocity at the frequency of Unity's physics system
    /// </summary>
    private void FixedUpdate()
    {
        if (_parent.MovementEnabled)
        {
            Flock();
        }
    }

    /// <summary>
    /// Applies acceleration to the Agent based on the assigned flocking behaviours
    /// </summary>
    private void Flock()
    {
        List<Transform> neighbourhood = GetNearbyObjects(_parent.Neighbours);

        // Create an acceleration value by adding the Vector3 results for each behaviour
        Vector3 acceleration = new Vector3();

        float totalWeight = 0;

        foreach (IBehaviour behaviour in _behaviours)
        {
            BehaviourResult result = behaviour.CalculateDirection(transform, neighbourhood);

            acceleration += result.WeightedDirection;

            totalWeight += result.Weight;
        }

        // Divide the result by the total weightings of all behaviours
        if (totalWeight > 0)
        {
            acceleration /= totalWeight;
        }

        _rigidbody.velocity += acceleration * _parent.AccelerationRate;

        // Cap the total velocity to the maximum speed defined by the parent BaseUnit
        if (_rigidbody.velocity.magnitude > _parent.MaximumSpeed)
        {
            _rigidbody.velocity = _rigidbody.velocity.normalized * _parent.MaximumSpeed;
        }

        // Look towards the direction we're moving in
        if (_rigidbody.velocity.magnitude > 0.0005f)
        {
            transform.rotation = Quaternion.LookRotation(_rigidbody.velocity);
        }
    }

    /// <summary>
    /// Checks all objects in a list and returns those within the maximum range defined by the parent BaseUnit
    /// </summary>
    /// <param name="neighbours">The Transforms to evaluate</param>
    /// <returns>The sublist of Transforms that are within range</returns>
    private List<Transform> GetNearbyObjects(List<Transform> neighbours)
        => neighbours.Where(neighbour => Vector3.Distance(neighbour.position, transform.position) <= _parent.MaximumRange && neighbour.position != transform.position).ToList();
}