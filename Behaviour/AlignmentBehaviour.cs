using System;
using UnityEngine;

/// <summary>
/// Behaviour for alignment between flockmates
/// </summary>
public class AlignmentBehaviour : Behaviour
{
    /// <summary>
    /// Assigns parameters to the behaviour fields
    /// </summary>
    /// <param name="range">The range used when determining which Transforms are close enough to act upon</param>
    /// <param name="weight">The importance of this behaviour which, acts as a multiplier to the final output</param>
    public AlignmentBehaviour(Func<float> range, Func<float> weight) : base(range, weight, Tags.ENTITYTAG) { }

    /// <summary>
    /// Calculates the normalised velocity of the neighbour divided by their distance and adds it to the average
    /// </summary>
    /// <param name="average">The current average velocity calculated by the Behaviour</param>
    /// <param name="neighboursCounted">The number of neighbours that have already been evaluated</param>
    /// <param name="self">The Transform of the Entity that called this method</param>
    /// <param name="neighbour">The Transform of the neighbour to be evaluated</param>
    /// <returns>The new Vector3 average velocity</returns>
    protected override Vector3 AddToAverage(Vector3 average, int neighboursCounted, Transform self, Transform neighbour)
    {
        Vector3 retVal = neighbour.GetComponent<Rigidbody>().velocity.normalized / Vector3.Distance(neighbour.position, self.position);

        // Recalculate average
        return ((average * neighboursCounted) + retVal) / (neighboursCounted + 1);
    }
}
