using System;
using UnityEngine;

/// <summary>
/// Behaviour for the flock evasion of GameOject future positions
/// </summary>
public class EvadeBehaviour : Behaviour
{

    /// <summary>
    /// Assigns parameters to the behaviour fields
    /// </summary>
    /// <param name="range">The range used when determining which Transforms are close enough to act upon</param>
    /// <param name="weight">The importance of this behaviour which, acts as a multiplier to the final output</param>
    /// <param name="framesInAdvance">How far in the future the neighbour position is prediced</param>
    public EvadeBehaviour(Func<float> range, Func<float> weight, Func<float> framesInAdvance) : base(range, weight, Tags.EVADETARGETTAG)
    {
        _framesInAdvance = framesInAdvance;
    }

    private readonly Func<float> _framesInAdvance;

    /// <summary>
    /// Calculates a desired velocity based on one given neighbour and adds it to the total average
    /// </summary>
    /// <param name="average">The current average velocity calculated by the Behaviour</param>
    /// <param name="neighboursCounted">The number of neighbours that have already been evaluated</param>
    /// <param name="self">The Transform of the Entity that called this method</param>
    /// <param name="neighbour">The Transform of the neighbour to be evaluated</param>
    /// <returns>Calculates a desired velocity based on one given neighbour and adds it to the total average</returns>
    protected override Vector3 AddToAverage(Vector3 average, int neighboursCounted, Transform self, Transform neighbour)
    {
        Vector3 neighbourFuturePosition = new Vector3();

        if (neighbour.GetComponent<Rigidbody>() != null)
        {
            neighbourFuturePosition = neighbour.position
                                      + neighbour.GetComponent<Rigidbody>().velocity * _framesInAdvance() * Time.deltaTime
                                      - self.position;
        }

        // Recalculate average
        return average - neighbourFuturePosition;
    }

}
