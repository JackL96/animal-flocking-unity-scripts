using System;
using UnityEngine;

/// <summary>
/// Behaviour for the flock pursuit of GameOject future positions
/// </summary>
public class PursuitBehaviour : Behaviour
{
    /// <summary>
    /// Assigns a parameter to the behaviour field
    /// </summary>
    /// <param name="range">The range used when determining which Transforms are close enough to act upon</param>
    /// <param name="weight">The importance of this behaviour which, acts as a multiplier to the final output</param>
    /// <param name="framePredictionCount">The number of frames in advance to predict a neighbour's position</param>
    public PursuitBehaviour(Func<float> range, Func<float> weight, Func<float> framePredictionCount) : base(range, weight, Tags.PURSUETARGETTAG)
    {
        _framePredictionCount = framePredictionCount;
    }

    private readonly Func<float> _framePredictionCount;

    /// <summary>
    /// Calculates a desired velocity based on one given neighbour and adds it to the total average
    /// </summary>
    /// <param name="average">The current average velocity calculated by the Behaviour</param>
    /// <param name="neighboursCounted">The number of neighbours that have already been evaluated</param>
    /// <param name="self">The Transform of the Entity that called this method</param>
    /// <param name="neighbour">The Transform of the neighbour to be evaluated</param>
    /// <returns>Calculates a desired velocity based on one given neighbour and adds it to the total average</returns>
    protected override Vector3 AddToAverage(Vector3 average, int neighboursCounted, Transform self, Transform neighbour)
    {
        Vector3 neighbourFuturePosition = new Vector3();

        if (neighbour.GetComponent<Rigidbody>() != null)
        {
            neighbourFuturePosition = neighbour.position
                                      + neighbour.GetComponent<Rigidbody>().velocity * _framePredictionCount() * Time.deltaTime
                                      - self.position;
        }

        // Recalculate average
        return average + neighbourFuturePosition;
    }
}
