using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Behaviour that creates and returns random noise
/// </summary>
public class NoiseBehaviour : IBehaviour
{
    /// <summary>
    /// Assigns a parameter to the behaviour field
    /// </summary>
    /// <param name="weight">The importance of this behaviour, which acts as a multiplier to the final output</param>
    public NoiseBehaviour(Func<float> weight)
    {
        _weight = weight;
    }

    private readonly Func<float> _weight;

    /// <summary>
    /// Generates random numbers to use as noise
    /// </summary>
    private static System.Random random = new System.Random();

    public float Weight
    {
        get
        {
            return _weight();
        }
    }

    /// <summary>
    /// Returns a Vector3 with random values from -1 to 1
    /// </summary>
    /// <param name="self">The Transform of the Entity that called this method (unused)</param>
    /// <param name="neighbours">The List of Transforms to consider during evaulation (unused)</param>
    /// <returns>A random Vector3 multiplied by the weight of the behaviour</returns>
    public BehaviourResult CalculateDirection(Transform self, List<Transform> neighbours)
    {
        return new BehaviourResult(new Vector3((float)(random.NextDouble() - 0.5f) * 2,
                                   0,
                                   (float)(random.NextDouble() - 0.5f) * 2), _weight());
    }
}
