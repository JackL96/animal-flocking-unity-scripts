using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public abstract class Behaviour : IBehaviour
{
    /// <summary>
    /// Assigns parameters to the behaviour fields
    /// </summary>
    /// <param name="range">The range used when determining which Transforms are close enough to act upon</param>
    /// <param name="weight">The importance of this behaviour which, acts as a multiplier to the final output</param>
    /// <param name="tag">The type of neighbours to consider for this behaviour</param>
    public Behaviour(Func<float> range, Func<float> weight, string tag)
    {
        _range = range;

        _weight = weight;

        _tag = tag;
    }

    private readonly Func<float> _range;

    private readonly Func<float> _weight;

    private readonly string _tag;

    /// <summary>
    /// Calculate velocity based on properties of the calling Entity and surrounding neighbours
    /// </summary>
    /// <param name="self">The Transform of the Entity that called this method</param>
    /// <param name="neighbours">The List of Transforms to consider during evaulation</param>
    /// <returns>The final velocity calculated by the behaviour, along with the weighting</returns>
    public BehaviourResult CalculateDirection(Transform self, List<Transform> neighbours)
    {
        if (_weight() == 0)
        {
            return new BehaviourResult();
        }

        Vector3 average = new Vector3();

        int neighboursCounted = 0;

        // For each correctly tagged Transform within range, calculate a velocity and add to the rolling average
        foreach (Transform neighbour in neighbours.Where(t => t.gameObject.tag == _tag))
        {
            if (Vector3.Distance(neighbour.position, self.position) <= _range())
            {
                average = AddToAverage(average, neighboursCounted, self, neighbour);

                neighboursCounted++;
            }
        }

        return new BehaviourResult(average, _weight());
    }

    /// <summary>
    /// Calculates a desired velocity based on one given neighbour and adds it to the total average
    /// </summary>
    /// <param name="average">The current average velocity calculated by the Behaviour</param>
    /// <param name="neighboursCounted">The number of neighbours that have already been evaluated</param>
    /// <param name="self">The Transform of the Entity that called this method</param>
    /// <param name="neighbour">The Transform of the neighbour to be evaluated</param>
    /// <returns>The Vector3 result to be added to the rolling average</returns>
    protected abstract Vector3 AddToAverage(Vector3 average, int neighboursCounted, Transform self, Transform neighbour);
}
