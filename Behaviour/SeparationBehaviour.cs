using System;
using UnityEngine;

/// <summary>
/// Behaviour for flock separation from a given List of Transforms
/// </summary>
public class SeparationBehaviour : Behaviour
{
    /// <summary>
    /// Assigns parameters to the behaviour fields
    /// </summary>
    /// <param name="range">The range used when determining which Transforms are close enough to act upon</param>
    /// <param name="weight">The importance of this behaviour which, acts as a multiplier to the final output</param>
    /// <param name="tag">The type of neighbours to consider for this behaviour</param>
    public SeparationBehaviour(Func<float> range, Func<float> weight, string tag) : base(range, weight, tag) { }

    /// <summary>
    /// Calculates the opposite unit vector to the position of the neighbour
    /// </summary>
    /// <param name="average">The current average velocity calculated by the Behaviour</param>
    /// <param name="neighboursCounted">The number of neighbours that have already been evaluated</param>
    /// <param name="self">The Transform of the Entity that called this method</param>
    /// <param name="neighbour">The Transform of the neighbour to be evaluated</param>
    /// <returns>Calculates a desired velocity based on one given neighbour and adds it to the total average</returns>
    protected override Vector3 AddToAverage(Vector3 average, int neighboursCounted, Transform self, Transform neighbour)
    {
        Vector3 oppDir = self.position - neighbour.position;

        // Add to average without recalculating
        return average + (oppDir.normalized / Vector3.Distance(neighbour.position, self.position));
    }
}
