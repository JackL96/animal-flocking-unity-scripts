using System;
using UnityEngine;

/// <summary>
/// Behaviour for flock cohesion towards a given List of Transforms
/// </summary>
public class CohesionBehaviour : Behaviour
{
    /// <summary>
    /// Assigns parameters to the behaviour fields
    /// </summary>
    /// <param name="range">The range used when determining which Transforms are close enough to act upon</param>
    /// <param name="weight">The importance of this behaviour, which acts as a multiplier to the final output</param>
    /// <param name="tag">The type of neighbours to consider for this behaviour</param>
    public CohesionBehaviour(Func<float> range, Func<float> weight, string tag) : base(range, weight, tag) { }

    protected override Vector3 AddToAverage(Vector3 average, int neighboursCounted, Transform self, Transform neighbour)
    {
        Vector3 retVal = neighbour.position - self.position;

        return ((average * neighboursCounted) + retVal) / (neighboursCounted + 1);
    }
}
