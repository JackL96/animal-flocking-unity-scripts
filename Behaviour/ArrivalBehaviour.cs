using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArrivalBehaviour : IBehaviour
{
    /// <summary>
    /// Assigns parameters to the behaviour fields
    /// </summary>
    /// <param name="range">The range used when determining which Transforms are close enough to act upon</param>
    /// <param name="stopRange">The range used when determining which Transforms are close enough to stop at</param>
    /// <param name="weight">The importance of this behaviour which, acts as a multiplier to the final output</param>
    /// <param name="tag">The type of neighbours to consider for this behaviour</param>
    public ArrivalBehaviour(Func<float> range, Func<float> weight, string tag)
    {
        _range = range;

        _weight = weight;

        _tag = tag;
    }

    private readonly Func<float> _range;

    private readonly Func<float> _weight;

    private readonly string _tag;

    /// <summary>
    /// Calculate velocity based on properties of the calling Entity and surrounding neighbours
    /// </summary>
    /// <param name="self">The Transform of the Entity that called this method</param>
    /// <param name="neighbours">The List of Transforms to consider during evaulation</param>
    /// <returns>The final velocity calculated by the behaviour, along with the weighting</returns>
    public BehaviourResult CalculateDirection(Transform self, List<Transform> neighbours)
    {
        // Distance to closest target
        float distance = neighbours.Where(t => t.gameObject.tag == _tag).Select(n => Vector3.Distance(n.position, self.position)).Append(float.MaxValue).Min();

        float ratio = Math.Max(_range() - distance, 0) / _range();

        float weight = (_weight() * ratio);

        return new BehaviourResult(Vector3.zero, weight);
    }
}

// This is my own implementation of the Enumerable.Append() method from System.Linq
// It's a .NET 4.8 addition but Unity is stuck at 4.0, so it was otherwise inaccessible
internal static class Extensions
{
    /// <summary>
    /// Appends an item of type <T> to a given IEnumerable<T>
    /// </summary>
    public static IEnumerable<T> Append<T>(this IEnumerable<T> source, T item)
    {
        foreach (T i in source)
        {
            yield return i;
        }

        yield return item;
    }
}