using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Package for returning behaviour calculation data
/// </summary>
public struct BehaviourResult
{
    /// <summary>
    /// The direction the behaviour wants to move the agent
    /// </summary>
    public Vector3 Direction { get; }

    /// <summary>
    /// The weighting of the behaviour
    /// </summary>
    public float Weight { get; }

    public Vector3 WeightedDirection { get { return Direction * Weight; } }

    public BehaviourResult(Vector3 direction, float weight)
    {
        Direction = direction;

        Weight = weight;
    }
}


/// <summary>
/// Generic interface for behaviours to implement
/// </summary>
public interface IBehaviour
{
    BehaviourResult CalculateDirection(Transform self, List<Transform> neighbours);
}
