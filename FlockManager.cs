using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Allows a GameObject to manage a flock, which consists of child agents
/// </summary>
public class FlockManager : MonoBehaviour
{
    [Header("Flocking Base Movement")]
    [SerializeField]
    private bool _movementEnabled = false;
    /// <summary>
    /// Determines if the flock may perform movement calculation or not
    /// </summary>
    public bool MovementEnabled { get { return _movementEnabled; } set { _movementEnabled = value; } }

    [Range(0, 10)]
    [SerializeField]
    private float _maximumSpeed = 2.5f;
    /// <summary>
    /// The maximum speed at which the flock can move
    /// </summary>
    public float MaximumSpeed { get { return _maximumSpeed; } private set { _maximumSpeed = value; } }

    [Range(0, 10)]
    [SerializeField]
    private float _accelerationRate = 5f;
    /// <summary>
    /// The rate at which the flock accelerates
    /// </summary>
    public float AccelerationRate { get { return _accelerationRate; } private set { _accelerationRate = value; } }

    #region Weights

    [Space(5)]
    [Range(0, 10)]
    [SerializeField]
    private float _alignmentWeight = 1;
    /// <summary>
    /// The weighting of the alignment behaviour of the flock
    /// </summary>
    public float AlignmentWeight { get { return _alignmentWeight; } private set { _alignmentWeight = value; } }

    [Range(0, 10)]
    [SerializeField]
    private float _separationWeight = 1;
    /// <summary>
    /// The weighting of the separation behaviour of the flock
    /// </summary>
    public float SeparationWeight { get { return _separationWeight; } private set { _separationWeight = value; } }

    [Range(0, 10)]
    [SerializeField]
    private float _cohesionWeight = 1;
    /// <summary>
    /// The weighting of the cohesion behaviour of the flock
    /// </summary>
    public float CohesionWeight { get { return _cohesionWeight; } private set { _cohesionWeight = value; } }

    [Range(0, 1)]
    [SerializeField]
    private float _noiseWeight = 0.1f;
    /// <summary>
    /// The weighting of the random noise added to the flock
    /// </summary>
    public float NoiseWeight { get { return _noiseWeight; } private set { _noiseWeight = value; } }

    [Header("Flocking Steer Behaviours")]

    [Range(0, 10)]
    [SerializeField]
    private float _collisionAvoidanceWeight = 5;
    /// <summary>
    /// The weighting of the collision avoidance of the flock
    /// </summary>
    public float CollisionAvoidanceWeight { get { return _collisionAvoidanceWeight; } private set { _collisionAvoidanceWeight = value; } }

    [Range(0, 1)]
    [SerializeField]
    private float _seekWeight = 0.1f;
    /// <summary>
    /// The weighting of the seek behaviour of the flock
    /// </summary>
    public float SeekWeight { get { return _seekWeight; } private set { _seekWeight = value; } }

    [Range(0, 10)]
    [SerializeField]
    private float _fleeWeight = 2.5f;
    /// <summary>
    /// The weighting of the flee behaviour of the flock
    /// </summary>
    public float FleeWeight { get { return _fleeWeight; } private set { _fleeWeight = value; } }

    [Range(0, 1)]
    [SerializeField]
    private float _evadeWeight = 0.1f;
    /// <summary>
    /// The weighting of the evade behaviour of the flock
    /// </summary>
    public float EvadeWeight { get { return _evadeWeight; } private set { _evadeWeight = value; } }

    [Range(0, 1)]
    [SerializeField]
    private float _pursueWeight = 0.1f;
    /// <summary>
    /// The weighting of the pursue behaviour of the flock
    /// </summary>
    public float PursueWeight { get { return _pursueWeight; } private set { _pursueWeight = value; } }

    [Range(0, 1000)]
    [SerializeField]
    private float _arrivalWeight = 0.1f;
    /// <summary>
    /// The weighting of the arrival behaviour of the flock
    /// </summary>
    public float ArrivalWeight { get { return _arrivalWeight; } private set { _arrivalWeight = value; } }

    [Range(0, 10000)]
    [SerializeField]
    private float _arrivalStopWeight = 25f;
    /// <summary>
    /// The weighting of the arrival behaviour of the flock when stopping
    /// </summary>
    public float ArrivalStopWeight { get { return _arrivalStopWeight; } private set { _arrivalStopWeight = value; } }

    #endregion

    #region Ranges

    /// <summary>
    /// The maximum range that children may search for other Transforms
    /// </summary>
    public float MaximumRange { get; private set; }

    /// <summary>
    /// The range at which children will align their direction with the group
    /// </summary>
    public float AlignmentRange { get; private set; }

    /// <summary>
    /// The range at which children will begin to avoid crowding
    /// </summary>
    public float SeparationRange { get; private set; }

    /// <summary>
    /// The range at which children will begin to group together
    /// </summary>
    public float CohesionRange { get; private set; }

    /// <summary>
    /// The range that children may search up to for Transforms they wish to avoid colliding with
    /// </summary>
    public float CollisionAvoidanceRange { get; private set; }

    /// <summary>
    /// The range that children may search up to for Transforms they wish to seek
    /// </summary>
    public float SeekRange { get; private set; }

    /// <summary>
    /// The range that children may search up to for Transforms they wish to flee from
    /// </summary>
    public float FleeRange { get; private set; }

    /// <summary>
    /// The range that children may search up to for Transforms they wish to evade
    /// </summary>
    public float EvadeRange { get; private set; }

    /// <summary>
    /// The range that children may search up to for Transforms they wish to pursue
    /// </summary>
    public float PursueRange { get; private set; }

    /// <summary>
    /// The range that children may search up to for Transforms they wish to seek for arrival
    /// </summary>
    public float ArrivalRange { get; private set; }

    /// <summary>
    /// The range that children begin stopping at Transforms they have arrived at
    /// </summary>
    public float ArrivalStopRange { get; private set; }

    #endregion

    [Space(5)]
    [Range(0, 1000)]
    [SerializeField]
    private int framePredictionCount = 120;
    /// <summary>
    /// The number of frames in advance to predict the position of transforms for Evade and Pursue behaviours
    /// </summary>
    public int FramePredictionCount { get { return framePredictionCount; } private set { framePredictionCount = value; } }

    /// <summary>
    /// String array of tags used to add GameObjects to the Neighbours list
    /// </summary>
    private static readonly string[] tags = { Tags.ENTITYTAG, Tags.WALLTAG, Tags.SEEKTARGETTAG, Tags.FLEETARGETTAG, Tags.EVADETARGETTAG, Tags.PURSUETARGETTAG, Tags.ARRIVALTARGETTAG };

    /// <summary>
    /// Storage for the list of Transforms that child agents may search through for their behaviours
    /// </summary>
    public List<Transform> Neighbours { get; } = new List<Transform>();

    /// <summary>
    /// Initialisation for the FlockManager within the Unity scene
    /// </summary>
    private void Start()
    {
        // Set the values for flocking search range
        // TODO: Read these from a config file instead
        MaximumRange = 12;

        AlignmentRange = 3.3333f;

        SeparationRange = 2.5641f;

        CollisionAvoidanceRange = 2;

        SeekRange = 12;

        FleeRange = 12;

        EvadeRange = 12;

        PursueRange = 12;

        ArrivalRange = 12;

        ArrivalStopRange = 8;

        // Add to neighbours all children of the flock manager with the tags specified in tags.cs
        foreach (string tag in tags)
        {
            Neighbours.AddRange(transform.Cast<Transform>().Where(t => t.gameObject.tag == tag && tag != Tags.WALLTAG));
        }

        // Always add all walls to the neighbours list since these are universal between flocks
        Neighbours.AddRange(GameObject.FindGameObjectsWithTag(Tags.WALLTAG).Select(gameObject => gameObject.transform));
    }
}
